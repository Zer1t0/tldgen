# tldgen

Append TLD to words. Helps to perform domain enumeration.


## Example

Give a word to append TLDs:
```
$ tldgen contoso
contoso.aaa
contoso.aarp
contoso.abarth
contoso.abb
...
contoso.zippo
contoso.zm
contoso.zone
contoso.zuerich
contoso.zw
```

## Installation

```
pip install tldgen
```
